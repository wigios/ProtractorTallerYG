var helper = require('../../helper');

var CreateEmail = function(){

  this.txtBxPara = element(by.xpath('//textarea'));
  this.txtBxAsunto = element(by.xpath('//div[3]/input'));
  this.txtAreaMensaje= element(by.xpath('//div[2]/div/div/table/tbody/tr/td[2]/div[2]'));
  this.btnEnviar = element(by.xpath('//div[4]/table/tbody/tr/td/div/div[2]'));

  //Variables
  this.txtEMailPara = "IngresaCorreoCliente@gmail.com";
  this.txtEMailAsunto = "test";
  this.txtEMailMensaje = "test";

  this.createNewMail = function (){
    helper.waitUntilReady(this.txtBxPara);
    this.txtBxPara.click().sendKeys(this.txtEMailPara);
    browser.actions().sendKeys(protractor.Key.TAB).perform();
    this.txtBxAsunto.click().sendKeys(this.txtEMailAsunto);
    this.txtAreaMensaje.click();
    this.btnEnviar.click();
    browser.sleep(1000);
  }
}

module.exports = CreateEmail;

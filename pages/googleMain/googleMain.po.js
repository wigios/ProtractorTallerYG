var GoogleMain = function(){

  this.txtBxSearch = element(by.id('lst-ib'));
  this.buttonGoogleSearch = element(by.css('input[name="btnK"]'));
  this.lnkGmail = element(by.css('h3.r > a'));

  //Variables
  this.txtKeyword = "gmail";

  this.searchMail = function (){
    this.txtBxSearch.click().sendKeys(this.txtKeyword);
    browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    this.buttonGoogleSearch.click();
    this.lnkGmail.click();
  }
}

module.exports = GoogleMain;

var helper = require('../../helper');

var GmailMain = function(){

  this.btnCompose = element(by.xpath('//div[2]/div/div/div/div[2]/div/div/div/div/div/div'));
  this.linkVerMensaje = element(by.id('link_vsm'));
  this.btnCuenta = element(by.xpath('//div/a/span'));
  this.btnSalir = element(by.xpath('//div[4]/div[2]/a'));

  this.openNewMail = function (){
    helper.waitUntilReady(this.btnCompose);
    this.btnCompose.click();
  }

  this.openMessageSendt = function (){
    helper.waitUntilReady(this.linkVerMensaje);
    this.linkVerMensaje.click();
  }

  this.signOutMail = function (){
    helper.waitUntilReady(this.btnCuenta);
    this.btnCuenta.click();

    helper.waitUntilReady(this.btnSalir);
    this.btnSalir.click();
    browser.sleep(1000);
  }
}

module.exports = GmailMain;

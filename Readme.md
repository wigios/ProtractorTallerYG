User Story: 
-----------
I want to navigate to google search engine, searching for "gmail" email management and enter, so i could be able of send an email to victor.giraldo@yuxiglobal.com account, and validate that the message was sent.
--------------------------------------------------------------------------------------------------------------------------------------

Escenario: Enviar un correo electr�nico de prueba desde mi cuenta Gmail a un usuario.

Caso de prueba: Buscar la plataforma de correo electr�nico de gmail.

	Descripci�n: Buscar la plataforma de Gmail desde la pagina principal 
	de Google.

	Precondiciones: 
	* tener instalado el navegador Google Chrome.
	* ingresar al navegador como incognito.

	1. Abrir navegador Google Chrome.
	2. Digitar la url: www.google.com
	3. Click en el campo de texto Buscar y digitar: gmail
	4. Click en el bot�n 'Buscar en Google'
	5. En la lista que aparece, dar click en el primer link 
	   de la lista con el nombre: 'Gmail - Google'.
	   
	Resultado esperado: 
	El label asociado al campo de texto de la cuenta de correo es: 
	Email or phone (Correo electr�nico o tel�fono)
	
	postcodiciones: 
	La p�gina actual debe estar solicitando que se ingrese la cuenta de correo 
	de gmail asignada. 

Caso de prueba: Ingresar a la cuenta de correo de Gmail con usuario y contrase�a.

	Descripci�n: Ingresar a la plataforma de Gmail con su propio usuario 
	y contrase�a que tiene asignado, ejemplo: user = auot@gmail.com, 
	el sufijo siempre termina con @gmail.com

	Precondiciones: * Estar ubicado en la pagina que me solicita el usuario de Gmail.
	* el usuario y contrase�a no deben estar guardados en memoria.
	
	1. En el campo de texto Email, digitar la cuenta de 
	   correo electr�nico de gmail asignada, click en el bot�n Next.
	2. En el siguiente campo de texto, ingresa la contrase�a 
	   v�lida para esa cuenta de correo. click en Next.
	   
	Resultado esperado: 
	Se visualiza correctamente el bot�n Compose(Redactar)
	
	postcodiciones: 
	La p�gina actual debe ser el ingreso a la cuenta de correo 
    en la secci�n de recibidos.
	   
Caso de prueba: Crear y enviar un nuevo correo electr�nico desde Gmail.
	Precondiciones: Estar dentro de la plataforma de Gmail
	* El bot�n Compose(Redactar) debe estar visualizado.
	
	Descripci�n: Luego de ingresar a la plataforma de correos de gmail, se crea un
	nuevo correo electr�nico, se llenan los datos principales, Para, Asunto, Mensaje, 
	y luego se env�a a su destinatario. 
	
	1. Una vez ingresado a su cuenta de correo, click en el bot�n Compose (Redactar).
	2. En la ventana que dice mensaje nuevo, en el campo To (Para), ingresa 
	   la cuenta a la que le va a enviar el correo. 
	3. En el campo de texto Subject (Asunto) ingresa la palabra 'test'. 
	4. En el area de texto que se encuentra debajo, ingresa la palabra 'test'. 
	5. click en el bot�n Send (enviar). 
	6. Luego de enviar el correo, verificar que se muestra un link con el 
	   nombre 'Se ha enviado el mensaje. Ver mensaje'
	7. click en el link anterior. 
	8. Se puede ver que entramos en el correo creado que se ha enviado.

	Resultado esperado: 
	validar que se visualiza el link: 'Se ha enviado el mensaje. Ver mensaje'	
	
	postcodiciones: 
	La p�gina actual debe estar en la secci�n de enviados con el correo
	que se acab� de construir.

Caso de prueba: Cerrar la sesi�n de mi cuenta de correo electr�nico de Gmail.

	Descripci�n: Se crea este escenario con el fin de cerrar el ciclo de la prueba 
	y se cierre correctamente la sesi�n del usuario gmail, se emplean dos botones 
	para salir y luego debe salir solicitandole la contrase�a nuevamente.
	
	Precondiciones: Debo estar dentro de mi cuenta de correo electronico de gmail.

	1. Una vez se realiz� el proceso, se da click en el bot�n 'Cuenta de google'
	   situado en la parte superior derecha de la pantalla, con una imagen reconocida. 
	2. Se abre un ventana, y se le da click al bot�n 'Salir'
	
	postcodiciones: 
	El sistema me debe solicitar nuevamente una contrase�a.
	
	Resultado esperado: 
	validar que el t�tulo de la p�gina actual es 'Gmail'.


--------------------------------------------------------------------------------------------------------------------------

--------------------
Guide for your test.
--------------------

Clone repo:

git clone https://gitlab.com/wigios/ProtractorTallerYG.git
You can use npm to install Protractor globally with:

npm install -g protractor
...or after cloning inside project folder execute: (that will install all dependencies from package.json)

npm i
The webdriver-manager is a helper tool to easily get an instance of a Selenium Server running. 

Use it to download the necessary binaries with:
webdriver-manager update
webdriver-manager start

-------------
Running Tests
-------------

Tests need to be run from the project directory inside the ProtractorTallerYG:
cd */ProtractorTallerYG  (*) path where the project is located.

Run Selenium in one tab via:
webdriver-manager start

Once the Selenium server is running, run this command in another tab:
protractor config.js

It is possible to run tests by test suites/groups separately:


protractor config.js --suite sendMail

------------
04. Questions: 
------------

Q1: If you need to run the Protractor test scenario developed on "02. Expected.txt", from a Jenkins Server running on a platform with User Interface (example Windows), You would do it?

one answer could be the follow: 

Creating Jenkins Job

Install node js on Jenkins Server
Install Html Publisher Plugin for end to end testing report
Create Freestyle Project or whatever your needs
Go to Build Section -> Add build step and choose Execute Windows batch command if Jenkins server in Windows otherwise choose Execute Shell for Linux
Call conf.js (install packages and call your configuration file)
For reporting Got to Post-Build Actions Section -> Add Publish Html Reports and call your report file (file assuming from root of your project)
However you can customize execution command using gulp or similar other packages.

Source: https://stackoverflow.com/questions/21338019/setting-up-continuous-integration-of-protractor-using-jenkins


Q2: If you need to run the Protractor test scenario developed on "02. Expected.txt", from a Jenkins Server running on a platform without User Interface (example Linux), You would do it? 

An example could be the next: 

The configuration is built upon following components:

Protractor tests � regular test suites that are fired on usual configuration with GUI
Grunt configuration to run protractor tests
Firefox browser installed on Jenkins� Linux Operating System (CentOS in my case)
Xvfb installed on Jenkins� host OS. Xvfb is a software that EMULATES GUI for non GUI systems
Grunt plugins set to run Protractor tests in a non GUI environment
Jenkins job configuration to run your job in a scheduled, regular manner
source: https://looksok.wordpress.com/2016/02/13/run-protractor-tests-in-jenkins-headless-browser-on-linux-with-xvfb/

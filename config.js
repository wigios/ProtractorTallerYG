// conf.js
exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['./tests/*.spec.js'],
  params:{
    url:'https://www.google.com'
  },
  suites:{
    sendMail: 'tests/*.spec.js'
  },
  jasmineNodeOpts: {
    defaultTimeoutInterval: 2500000
  },
  onPrepare: function(){
    browser.ignoreSynchronization  = true;
  }
}

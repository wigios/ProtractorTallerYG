var GoogleMain = require('../pages/googleMain/googleMain.po.js');
var LoginGmail = require('../pages/loginGmail/loginGmail.po.js');
var GmailMain = require('../pages/gmailMain/gmailMain.po.js');
var CreateEmail = require('../pages/createEmail/createEmail.po.js');

var googleMain = new GoogleMain();
var loginGmail = new LoginGmail();
var gmailMain = new GmailMain();
var createEmail = new CreateEmail();

describe('Send Email to an user from Gmail', function() {

  beforeEach(function(){
    browser.get(browser.params.url);
    browser.driver.manage().window().maximize();
  });

  afterEach(function(){
    browser.manage().deleteAllCookies();
  });

  it('should sign in and to send a mail to an user from gmail', function(){

    googleMain.searchMail();
    loginGmail.signInMail();
    gmailMain.openNewMail();
    createEmail.createNewMail();
    gmailMain.openMessageSendt();
    gmailMain.signOutMail();

    //La prueba fue exitosa
    expect(browser.getTitle()).toEqual("Gmail");

  });

});
